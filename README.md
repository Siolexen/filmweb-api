# Filmweb API v 0.3.1

## Wprowadzenie

Biblioteka szumnie nazwana API pozwala na pobranie wybranych informacji zgromadzonych w ramach serwisu Filmweb.

## Instalacja

Aby możliwe było korzystanie z metod biblioteki w projekcie należy:

* Dodać bibliotekę do lokalnego repozytorium Maven:

    mvn install:install-file -Dfile=FilmwebApi-0.3.1.jar -DgroupId=info.talacha -DartifactId=FilmwebApi -Dversion=0.3.1 -Dpackaging=jar

* Dodać do projektu zależność:

    <dependency>
        <groupId>info.talacha</groupId>
        <artifactId>FilmwebApi</artifactId>
        <version>0.3.1</version>
    </dependency>

## Przykład użycia

    /**
     * Tworzymy obiekt :)
     */
    FilmwebApi fa = new FilmwebApi();
    
    /**
     * Możemy wyszukać filmy, seriale lub gry o danym tytule (polskim lub oryginalnym)
     * opcjonalnie ograniczając dane do roku produkcji, np.:
     * - fa.findSeries(title) / fa.findSeries(title, year) - seriale
     * - fa.findGame(title) / fa.findGame(title, year) - gry
     * 
     * Pobieramy więc krótkie info o filmach z 1986 roku, których tytuł zawiera frazę "aliens"
     */
    List<FilmSearchResult> filmInfoList = fa.findFilm("aliens", 1986);
    
    for (FilmSearchResult film : filmInfoList) {
    
        /**
         * Możemy przejrzeć dane z wyszukiwarki... 
         */
        film.getId();           // ID filmu
        film.getTitle();        // Tytuł filmu
        film.getPolishTitle();  // Polski tytuł
        film.getImageURL();     // Adres URL plakatu
        
        /**
         * Mając ID możemy pobrać opisy pozycji
         */
        List<String> descList = fa.getDescriptions(film.getId());
        
        /**
         * Możemy również pobrać inne dane nt. filmu...
         * W przypadku serialu również licznę odcinków / sezonów, odpowiednio:
         * - getEpisodesCount()
         * - getSeasonsCount()
         */
        Film f = fa.getFilmData(film.getId());
        f.getCountries();   // Kraj produkcji
        f.getGenre();       // Gatunek
        f.getPlot();        // Zarys fabuły
        f.getDuration();    // Czas trwania (min)
        f.getRate();        // Średnia ocen
        f.getVotes();       // Liczba głosów
        f.getWebsiteURL();  // Adres URL strony
        
        /**
         * Sprawdzić kiedy będzie w telewizji
         */
        List<Broadcast> broadcasts = fa.getBroadcasts(film.getId(), 0, 20);
        for (Broadcast b : broadcasts) {
            
            b.getChannelId();   // ID kanału TV
            b.getDate();        // data emisji
            b.getTime();        // godzina emisji
            b.getDescription(); // krótkie info (gatunek, odcinek i sezon dla serialu)
        }
        
        /**
         * Albo dostać się do danych nt osób zaangażowanych w produkcję,
         * aktorów, scenarzystów, producentów itp. Np.:
         */
        List<Person> actors = fa.getPersons(film.getId(), Profession.ACTOR, 0, 50);
        for (Person p : actors) {
            
            p.getName();        // Imię i nazowsko
            p.getRole();        // Rola w filmie
            p.getInfo();        // Dodatkowe info
            p.getPhotoUrl();    // Adres URL zdjęcia
        }
    }
    
    /**
     * Da się pobrać listę kanałów TV
     */
     List<TVChannel> tvChannels = fa.getTvChannels();
     for (TVChannel tvChannel : tvChannels) {
          
          tvChannel.getId();                // ID kanału TV
          tvChannel.getName();              // nazwa kanału
          tvChannel.getLogo(Size.SMALL);    // logo (małe)
     }
    
    /**
     * Poza tym istnieje możliwość zalogowania się
     */
    User user = fa.login("nasz_login", "tajne_haslo");
    user.getId();           // ID użytkownika
    user.getName();         // Nazwa
    user.getUsername();     // Imię i nazwisko
    user.getAvatarURL();    // Adres URL awatara
    
    /**
     * Zalogowany użytkownik może sprawdzić własne oceny...
     */
     List<Vote> votes = fa.getUserVotes(user.getId(), 0, 20);
     for (Vote vote : votes) {
     
          vote.getType();        // czy ocena filmu / serialu / gry
          vote.getItemId();      // ID ocenianej pozycji (odpowiednio do typu)
          vote.getDate();        // data oceny
          vote.getRate();        // ocena (1-10)
          vote.getComment();     // komentarz do oceny
          vote.isFavourite();    // czy dodano do ulubionych
     
     }
     
    /**
     * pozycje do obejrzenia / zagrania
     */
     List<WatchlistItem> watchList = fa.getUserWatchlist(user.getId(), 0, 20);
     for (WatchlistItem item : watchList) {
         
          item.getType();        // czy film / serial / gra
          item.getItemId();      // ID pozycji
          item.getDate();        // data dodania na listę
          item.getLevel();       // poziom zainteresowania (1-5)
     }
     
    /**
     * oraz zbiór ID własnych kanałów TV
     */
     Set<Long> tvChannelIdSet = fa.getUserTvChannels(user.getId());


## Licencja

Biblioteka udostępniona jest na licencji [CC BY 3.0][1], możliwe jest więc kopiowanie, rozpowszechnianie, modyfikowanie
i dowolne użycie pod warunkiem umieszczenia w widocznym miejscu informacji o autorze i pochodzeniu programu.

[1]: http://creativecommons.org/licenses/by/3.0/pl/