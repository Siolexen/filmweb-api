package info.talacha.filmweb.connection;

/**
 * Dozwolone w API metody HTTP
 * @author Paweł Talacha
 */
public enum HttpMethod {
	GET, POST
}