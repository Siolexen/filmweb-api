package info.talacha.filmweb.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

/**
 * Lista do obejrzenia / zagrania 
 * @author Paweł Talacha
 */
public class WatchlistItem implements Serializable {

    private static final long serialVersionUID = 2057545650501269129L;

    private Long itemId;
    private LocalDate date;
    private ItemType type;
    private int level;
    
    public WatchlistItem(List<String> data) {
        this.prepare(data);
    }
    
    /**
     * ID na liście do obejrzenia 
     */
    public Long getItemId() {
        return itemId;
    }

    /**
     * Data dodania na listę
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Typ pozycji - film / serial / gra
     */
    public ItemType getType() {
        return type;
    }

    /**
     * Poziom zainteresowania
     */
    public int getLevel() {
        return level;
    }

    private void prepare(List<String> data) {
        this.itemId = Long.parseLong(data.get(0));
        
        Timestamp stamp = new Timestamp(Long.parseLong(data.get(1)));
        this.date = stamp.toLocalDateTime().toLocalDate();

        this.level = Integer.parseInt(data.get(2));

        this.type = ItemType.getType(Integer.parseInt(data.get(3)));
    }
}