package info.talacha.filmweb.search;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.talacha.filmweb.connection.Connection;
import info.talacha.filmweb.connection.HttpMethod;
import info.talacha.filmweb.search.models.FilmSearchResult;
import info.talacha.filmweb.search.models.ItemSearchResult;
import info.talacha.filmweb.search.models.PersonSearchResult;
import info.talacha.filmweb.search.models.SearchResult;
import info.talacha.filmweb.settings.Config;

/**
 * Wyszukiwarka danych
 * @author Paweł Talacha
 */
public class LiveSearch {

	private static final Logger LOGGER = LoggerFactory.getLogger(LiveSearch.class);
	
    private String getResult(String param) {

        StringBuilder html = new StringBuilder();
        try (
    		Connection conn = new Connection(Config.LIVE_SEARCH_URL + URLEncoder.encode(param, "UTF-8"), HttpMethod.GET);
    		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF8"))) {

            String str;
            while ((str = br.readLine()) != null) {
                html.append(str);
            }
            br.close();
        } catch (MalformedURLException e) {
            LOGGER.error("Nieprawidłowy adres URL", e);
        }
        catch (Exception e) {
        	LOGGER.error("Błąd pobierania danych", e);
		}
        LOGGER.debug("Resp: " + html.toString());
        return html.toString();
    }
    
    private List<SearchResult> prepareResultList(String result) {
    	List<SearchResult> resultList = new ArrayList<>();
    	String[] elementList = result.split(Config.LIVE_SEARCH_ROW_SPACER);
    	for (String element : elementList) {
    		resultList.add(this.prepareResult(element));
    	}
    	return resultList;
    }
    
    private SearchResult prepareResult(String element) {
        String[] elementData = element.split(Config.LIVE_SEARCH_FIELD_SPACER);
        
        SearchResult result;
        char type = elementData[0].charAt(0);
        if (type == 'f' || type == 's') {
            result = new FilmSearchResult(elementData);
        }
        else if (type == 'p') {
            result = new PersonSearchResult(elementData);
        }
        else {
            result = new ItemSearchResult(elementData);
        }
        result.setType(SearchResultType.valueByDiscriminant(type));
    	return result;
	}

	/**
	 * Metoda wyszukuje dane w bazie serwisu Filmweb
	 * @param query Zapytanie
	 * @return Skrócone informacje o pasujących obiektach
	 */
	public List<SearchResult> search(String query) {
		query = query.replaceAll("\\s", "+");
    	String res = this.getResult(query);
    	if (res.isEmpty()) {
    		return Collections.emptyList();
    	}
    	return this.prepareResultList(res);
    }
	
	/**
	 * Metoda wyszukuje dane w bazie serwisu Filmweb
	 * @param query Zapytanie
	 * @param type Typ oczekiwanego wyniku: film, serial, osoba, gra
	 * @return Skrócone informacje o pasujących obiektach
	 */
	public List<SearchResult> search(String query, SearchResultType type) {
		List<SearchResult> resList = this.search(query);
		List<SearchResult> resultList = new ArrayList<>();
		for (SearchResult result : resList) {
			if (result.getType().equals(type)) {
				resultList.add(result);				
			}
		}
		return resultList;
	}
}
