package info.talacha.filmweb.search;

/**
 * Typ "rekordu" zwracanego przez wyszukiwarkę
 * @author Paweł Talacha
 */
public enum SearchResultType {
	
	FILM('f'),
	SERIES('s'),
	PERSION('p'),
	GAME('g');
	
	private char discriminant;

	SearchResultType(final char discriminant) {
        this.discriminant = discriminant;
    }

    public int getDiscriminant() {
        return discriminant;
    }
    
    public static SearchResultType valueByDiscriminant(char discriminant) throws IllegalStateException {
    	for (SearchResultType type : SearchResultType.values()) {
    		if (type.discriminant == discriminant) {
    			return type;
    		}
    	}
    	throw new IllegalStateException("Nieprawidłowy typ: " + discriminant);
    }
}