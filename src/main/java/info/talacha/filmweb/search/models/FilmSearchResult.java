package info.talacha.filmweb.search.models;

/**
 * Dane wyszukiwarki dotyczące filmów / seriali
 * @author Paweł Talacha
 */
public class FilmSearchResult extends ItemSearchResult {
	
    private static final long serialVersionUID = 7319957560402101905L;

    private String cast;
	
	public FilmSearchResult(String[] data) {
		super(data);
		this.prepare(data);
	}

	public String getCast() {
		return cast;
	}

	public void setCast(String cast) {
		this.cast = cast;
	}
	
	private void prepare(String[] data) {
		if (data.length >= 8) {
			this.setCast(data[7]);
		}
	}
}