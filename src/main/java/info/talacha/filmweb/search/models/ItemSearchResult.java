package info.talacha.filmweb.search.models;

/**
 * Dane wyszukiwarki dotyczące obiektów z bazy Filmweb - filmów / seriali / gier
 * @author Paweł Talacha
 */
public class ItemSearchResult extends SearchResult {

    private static final long serialVersionUID = 1177971811359815614L;
    
    private String title;
	private String polishTitle;
	private String alternativeTitle;
	private Integer year;
	
	public ItemSearchResult(String[] data) {
		super(data);
		this.prepare(data);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPolishTitle() {
		return polishTitle;
	}

	public void setPolishTitle(String polishTitle) {
		this.polishTitle = polishTitle;
	}

	public String getAlternativeTitle() {
		return alternativeTitle;
	}

	public void setAlternativeTitle(String alternativeTitle) {
		this.alternativeTitle = alternativeTitle;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	private void prepare(String[] data) {
		this.setTitle(data[3]);
		this.setPolishTitle(data[4]);
		this.setAlternativeTitle(data[5]);
		this.setYear(Integer.parseInt(data[6]));
	}
}