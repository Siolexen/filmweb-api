package info.talacha.filmweb.search.models;

import info.talacha.filmweb.models.Profession;
import info.talacha.filmweb.settings.Config;

/**
 * Dane wyszukiwarki dotyczące osób
 * @author Paweł Talacha
 */
public class PersonSearchResult extends SearchResult {

    private static final long serialVersionUID = -8181280035899428463L;
    
    private String name;	//imię i nazwisko
	private Profession profession;
	private String title;	//tytuł + roku produkcji, z której jest znany
	
	public PersonSearchResult(String[] data) {
		super(data);
		this.prepare(data);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Profession getProfession() {
		return profession;
	}

	public void setProfession(Profession profession) {
		this.profession = profession;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	private void prepare(String[] data) {
		if (!data[2].isEmpty()) {
			this.setImageURL(Config.IMG_PERSON_URL + data[2]);
		}
		this.setName(data[3]);
		this.setProfession(Profession.valueByCode(Integer.parseInt(data[5])));
		if (data.length >= 7 && !data[6].isEmpty()) {
		    this.setTitle(data[6]);
		}
	}
}