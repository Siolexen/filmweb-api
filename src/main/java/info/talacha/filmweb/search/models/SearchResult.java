package info.talacha.filmweb.search.models;

import java.io.Serializable;

import info.talacha.filmweb.search.SearchResultType;
import info.talacha.filmweb.settings.Config;

/**
 * Dane z wyszukiwarki
 * @author Paweł Talacha
 */
public class SearchResult implements Serializable {

    private static final long serialVersionUID = -590397075987885277L;
    
    private Long id;
    private SearchResultType type;
    private String imageURL;
    
    public SearchResult(String[] data) {
        this.prepare(data);
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SearchResultType getType() {
		return type;
	}

	public void setType(SearchResultType type) {
		this.type = type;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
	private void prepare(String[] data) {
		this.setId(Long.parseLong(data[1]));
		if (!data[2].isEmpty()) {
			this.setImageURL(Config.IMG_POSTER_URL + data[2]);
		}
	}
}