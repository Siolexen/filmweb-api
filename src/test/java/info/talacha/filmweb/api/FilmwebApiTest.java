package info.talacha.filmweb.api;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import info.talacha.filmweb.connection.FilmwebException;
import info.talacha.filmweb.models.Broadcast;
import info.talacha.filmweb.models.Film;
import info.talacha.filmweb.models.Game;
import info.talacha.filmweb.models.ItemType;
import info.talacha.filmweb.models.Person;
import info.talacha.filmweb.models.Profession;
import info.talacha.filmweb.models.Series;
import info.talacha.filmweb.models.Size;
import info.talacha.filmweb.models.TVChannel;
import info.talacha.filmweb.search.models.FilmSearchResult;
import info.talacha.filmweb.search.models.ItemSearchResult;

/**
 * Testy działania metod API nie wymagających logowania
 * @author Paweł Talacha
 */
public class FilmwebApiTest {
    
    private static FilmwebApi fa;
    
    @BeforeClass
    public static void prepareApi() {
        fa = new FilmwebApi();
    }
    
    @Test
    public void testGetFilmList_CorrectTitle() {
    	String polishTitle = "Żandarm się żeni";
        List<FilmSearchResult> filmList = fa.findFilm(polishTitle);
        assertFalse("Nie znaleziono filmu o polskim tytule: " + polishTitle, filmList.isEmpty());
        assertEquals("Pobrano film o innym polskim tytule", polishTitle, filmList.get(0).getPolishTitle());
    }

    @Test
    public void testGetFilmList_CorrectTitleAndYear() {
    	String title = "Alien";
    	Integer year = 1979;
        List<FilmSearchResult> filmList = fa.findFilm(title, year);
        assertFalse("Brak listy filmów dla poprawnego tytułu i roku", filmList.isEmpty());
        assertEquals("Pobrano film o innym tytule", title, filmList.get(0).getTitle());
        assertEquals("Pobrano film z innego roku", year, filmList.get(0).getYear());
    }

    @Test
    public void testGetSeriesList_CorrectTitle() {
    	String title = "Twin Peaks";
    	List<FilmSearchResult> seriesList = fa.findSeries(title);
        assertFalse("Nie znaleziono serialu o tytule: " + title, seriesList.isEmpty());
        assertEquals("Pobrano serial o innym tytule", title, seriesList.get(0).getTitle());
    }
    
    @Test
    public void testGetSeriesList_CorrectTitleAndYear() {
    	String title = "Twin Peaks";
    	Integer year = 1990;
    	List<FilmSearchResult> seriesList = fa.findSeries(title, year);
        assertFalse("Nie znaleziono serialu o tytule: " + title, seriesList.isEmpty());
        assertEquals("Pobrano serial o innym tytule", title, seriesList.get(0).getTitle());
        assertEquals("Pobrano serial z innego roku", year, seriesList.get(0).getYear());
    }

    @Test
    public void testGetGameList_CorrectTitle() {
    	String title = "The Longest Journey";
    	List<ItemSearchResult> gameList = fa.findGame(title);
        assertFalse("Nie znaleziono gry o tytule: " + title, gameList.isEmpty());
        assertEquals("Pobrano grę o innym tytule", title, gameList.get(0).getTitle());
    }
    
    @Test
    public void testGetGameList_CorrectTitleAndYear() {
    	String title = "Grand Theft Auto V";
    	Integer year = 2013;
    	List<ItemSearchResult> gameList = fa.findGame(title);
        assertFalse("Nie znaleziono gry o tytule: " + title, gameList.isEmpty());
        assertEquals("Pobrano grę o innym tytule", title, gameList.get(0).getTitle());
        assertEquals("Pobrano grę z innego roku", year, gameList.get(0).getYear());
    }
    
    @Test
    public void testGetFilmPersons_IncorrectId() throws FilmwebException {
    	List<Person> persons = fa.getPersons(-10L, Profession.ACTOR, 0, 100);
    	assertTrue("Została pobrana lista aktorów", persons.isEmpty());
    }
    
    private boolean isPersonExist(List<Person> professionList, String name) {
    	for (Person person : professionList) {
    		if (person.getName().equals(name)) {
    			return true;
    		}
    	}
    	return false;
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceDirector() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "David Fincher";

    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.DIRECTOR, 0, 100);
    	assertTrue("Nie znaleziono reżysera", this.isPersonExist(persons, name));
    }
    
    @Test
    public void testGetFilmPersons_CorrectPreferenceScreenwriter() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Aaron Sorkin";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.SCREENWRITER, 0, 100);
    	assertTrue("Nie znaleziono scenarzysty", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceMusic() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Trent Reznor";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.MUSIC, 0, 100);
    	assertTrue("Nie znaleziono autora muzyki", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceCinematographer() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Jeff Cronenweth";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.CINEMATOGRAPHER, 0, 100);
    	assertTrue("Nie znaleziono autora zdjęć", this.isPersonExist(persons, name));
    }
    
    @Test
    public void testGetFilmPersons_CorrectPreferenceOriginalMaterials() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Ben Mezrich";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.ORIGINAL_MATERIALS, 0, 100);
    	assertTrue("Nie znaleziono autora materiałów do scenariusza", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceActor() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Jesse Eisenberg";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.ACTOR, 0, 100);
    	assertTrue("Nie znaleziono aktora", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceProducer() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Kevin Spacey";
    	
    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.PRODUCER, 0, 100);
    	assertTrue("Nie znaleziono producenta", this.isPersonExist(persons, name));
    }
    
    @Test
    public void testGetFilmPersons_CorrectPreferenceMontage() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Angus Wall";

    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.MONTAGE, 0, 100);
    	assertTrue("Nie znaleziono montażysty", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmPersons_CorrectPreferenceCostumeDesigner() throws FilmwebException {
    	long socialNetworkId = 528258;
    	String name = "Jacqueline West";

    	List<Person> persons = fa.getPersons(socialNetworkId, Profession.COSTUME_DESIGNER, 0, 100);
    	assertTrue("Nie znaleziono montażysty", this.isPersonExist(persons, name));
    }

    @Test
    public void testGetFilmDescription_CorrectId() throws FilmwebException {
    	long filmId = 528258;
    	List<String> descList = fa.getDescriptions(filmId);
    	assertTrue("Brak opisu filmu", descList.size() > 0);
    }

    @Test
    public void testGetSeriesDescription_CorrectId() throws FilmwebException {
    	long seriesId = 104386;
    	List<String> descList = fa.getDescriptions(seriesId);
    	assertTrue("Brak opisu serialu", descList.size() > 0);
    }

    @Test
    public void testGetGameDescription_CorrectId() throws FilmwebException {
    	long gameId = 607743;
    	List<String> descList = fa.getDescriptions(gameId);
    	assertTrue("Brak opisu gry", descList.size() > 0);
    }

    @Test
    public void testGetFilmData_CorrectId() throws FilmwebException {
    	long alienId = 980;
    	Film film = fa.getFilmData(alienId);
    	assertEquals("Błędny tytuł filmu", film.getTitle(), "Alien");
    	assertEquals("Błędny polski tytuł filmu", film.getPolishTitle(), "Obcy - 8. pasażer \"Nostromo\"");
    	assertEquals("Błędny rok produkcji filmu", film.getYear(), new Integer(1979));
    	assertThat("Nieprawidłowa okładka filmu", film.getPosterURL().toString(), endsWith(".jpg"));
    	assertEquals("Nieprawidłowy adres strony filmu", film.getWebsiteURL().toString(), "http://www.filmweb.pl/Obcy");
        assertTrue("Ocena filmu nie została pobrana", film.getRate() != null);
        assertTrue("Liczba głosów filmu nie została pobrana", film.getVotes() != null);
        assertEquals("Nieprawidłowy gatunek filmu", film.getGenre(), "Horror, Sci-Fi");
        assertEquals("Nieprawidłowy czas trwania filmu", film.getDuration(), new Integer(117));
        assertEquals("Nieprawidłowy kraj produkcji", film.getCountries(), "USA, Wielka Brytania");
        assertThat("Błędny zarys fabuły", film.getPlot(), startsWith("Załoga statku kosmicznego Nostromo"));
    }
    
    @Test
    public void testGetSeriesData_CorrectId() throws FilmwebException {
    	long dueSouthId = 104386;
    	Series series = fa.getSeriesData(dueSouthId);
    	assertEquals("Błędny tytuł serialu", series.getTitle(), "Due South");
    	assertEquals("Błędny polski tytuł serialu", series.getPolishTitle(), "Na Południe");
    	assertEquals("Błędny rok produkcji serialu", series.getYear(), new Integer(1994));
    	assertThat("Nieprawidłowa okładka serialu", series.getPosterURL().toString(), endsWith(".jpg"));
    	assertEquals("Nieprawidłowy adres strony serialu", series.getWebsiteURL().toString(), "http://www.filmweb.pl/serial/Na+Po%C5%82udnie-1994-104386");
        assertTrue("Ocena serialu nie została pobrana", series.getRate() != null);
        assertTrue("Liczba głosów serialu nie została pobrana", series.getVotes() != null);
        assertEquals("Nieprawidłowy gatunek serialu", series.getGenre(), "Przygodowy");
        assertEquals("Nieprawidłowy czas trwania serialu", series.getDuration(), new Integer(49));
        assertEquals("Nieprawidłowy kraj produkcji", series.getCountries(), "Kanada, USA");
        assertThat("Błędny zarys fabuły", series.getPlot(), startsWith("Konstabl"));
        assertEquals("Nieprawidłowa liczba sezonów", series.getSeasonsCount(), new Integer(4));
    	assertEquals("Nieprawidłowa liczba odcinków", series.getEpisodesCount(), new Integer(67));
    }

    @Test
    public void testGetGameData_CorrectId() throws FilmwebException {
    	long gameId = 607743;
    	Game game = fa.getGameData(gameId);
    	assertEquals("Błędny tytuł serialu", game.getTitle(), "The Longest Journey");
    	assertEquals("Błędny polski tytuł serialu", game.getPolishTitle(), "The Longest Journey: Najdłuższa podróż");
    	assertEquals("Błędny rok produkcji serialu", game.getYear(), new Integer(1999));
    	assertThat("Nieprawidłowa okładka serialu", game.getPosterURL().toString(), endsWith(".jpg"));
    	assertEquals("Nieprawidłowy adres strony serialu", game.getWebsiteURL().toString(), "http://www.filmweb.pl/videogame/The+Longest+Journey%3A+Najd%C5%82u%C5%BCsza+podr%C3%B3%C5%BC-1999-607743");
        assertTrue("Ocena serialu nie została pobrana", game.getRate() != null);
        assertTrue("Liczba głosów serialu nie została pobrana", game.getVotes() != null);
        assertEquals("Nieprawidłowy gatunek serialu", game.getGenre(), "Przygodowa");
        assertEquals("Nieprawidłowy kraj produkcji", game.getCountries(), "Norwegia");
        assertThat("Błędny zarys fabuły", game.getPlot(), startsWith("Rok 2209"));
    }

    @Test(expected=FilmwebException.class)
    public void testLogin_Incorrect() throws FilmwebException {
        fa.login("asdfghjkl", "assdffdssf");
    }


    @Test
    public void testGetBroadcasts() throws FilmwebException {
        fa.getBroadcasts(532928L, 0, 20);
    }

    @Test
    public void testGetBroadcastsData() {
        Broadcast broadcast = new Broadcast(Arrays.asList("532928","1","20:25","2016-02-14", "1234", "serial odc. 50", "2"));
        assertEquals("Błędny ID premiery TV", broadcast.getId(), new Long(1234));
        assertEquals("Błędny ID serialu", broadcast.getItemId(), new Long(532928));
        assertEquals("Błędna godzina emisji", broadcast.getTime(), LocalTime.parse("20:25"));
        assertEquals("Błędna data emisji", broadcast.getDate(), LocalDate.parse("2016-02-14"));
        assertEquals("Błędny opis", broadcast.getDescription(), "serial odc. 50");
        assertEquals("Błędny typ", broadcast.getType(), ItemType.SERIES);
    }

    @Test
    public void testGetTvChannels() throws FilmwebException {
        fa.getTvChannels();
    }
    
    @Test
    public void testGetTvChannelsData() throws MalformedURLException {
        TVChannel tv = new TVChannel(Arrays.asList("2","TVP 2","","5"));
        assertEquals("Błędny ID kanału TV", tv.getId(), new Long(2));
        assertEquals("Błędna nazwa kanału TV", tv.getName(), "TVP 2");
        assertEquals("Błędna godzina rozpoczęcia nadawania", tv.getDayStartHour(), 5);
        assertEquals("Błędny URL logo", tv.getLogo(Size.SMALL), new URL("http://1.fwcdn.pl/channels/2.0.png"));
        assertEquals("Błędny URL logo", tv.getLogo(Size.MEDIUM), new URL("http://1.fwcdn.pl/channels/2.1.png"));
        assertEquals("Błędny URL logo", tv.getLogo(Size.BIG), new URL("http://1.fwcdn.pl/channels/2.2.png"));
    }
}