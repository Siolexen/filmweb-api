package info.talacha.filmweb.api;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import info.talacha.filmweb.api.cache.Cache;
import info.talacha.filmweb.api.methodHandlers.GetUserVotesMthHandler;
import info.talacha.filmweb.api.methodHandlers.GetUserWatchlistMthHandler;
import info.talacha.filmweb.connection.FilmwebException;
import info.talacha.filmweb.models.ItemType;
import info.talacha.filmweb.models.User;
import info.talacha.filmweb.models.Vote;
import info.talacha.filmweb.models.WatchlistItem;

/**
 * Testy działania metod API wymagających logowania
 * @author Paweł Talacha
 */
public class FilmwebApiUserMethodsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FilmwebApiUserMethodsTest.class);
    
    private static FilmwebApi fa;
    private static User user;
    
    @BeforeClass
    public static void loadProperties() throws FilmwebException {
        fa = new FilmwebApi();
        Properties prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        try (InputStream resourceStream = loader.getResourceAsStream("user.properties")) {
            prop.load(resourceStream);
            user = fa.login(prop.getProperty("user.login"), prop.getProperty("user.pass"));
        } catch (IOException e) {
            LOGGER.error("Błąd pobierania ustawień", e);
        }
    }
    
    @Test
    public void testGetVotes() throws FilmwebException {
        fa.getUserVotes(user.getId(), 0, 20);
    }
    
    private List<Vote> getUserVotes(Cache cache, Long userId, int page, int limit) throws FilmwebException {
        try {
            GetUserVotesMthHandler handler = (GetUserVotesMthHandler)cache.getMthHandler(GetUserVotesMthHandler.class.getName());
            return handler.getEntry(userId, page, limit);
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return Collections.emptyList();
    }
    
    @Test
    public void testIsGetVotesPaginatorNotFixed() throws FilmwebException {
        Cache cache = new Cache();
        List<Vote> part1 = this.getUserVotes(cache, user.getId(), 0, 1);
        List<Vote> part2 = this.getUserVotes(cache, user.getId(), 1, 1);
        assertTrue("Naprawili paginator", part1.get(0).getItemId().equals(part2.get(0).getItemId()));
    }
    
    private List<WatchlistItem> getUserWatchlist(Cache cache, Long userId, int page, int limit) throws FilmwebException {
        try {
            GetUserWatchlistMthHandler handler = (GetUserWatchlistMthHandler)cache.getMthHandler(GetUserWatchlistMthHandler.class.getName());
            return handler.getEntry(userId, page, limit);
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return Collections.emptyList();
    }
    
    @Test
    public void testIsGetUserWatchlistPaginatorNotFixed() throws FilmwebException {
        Cache cache = new Cache();
        List<WatchlistItem> part1 = this.getUserWatchlist(cache, user.getId(), 0, 1);
        List<WatchlistItem> part2 = this.getUserWatchlist(cache, user.getId(), 1, 1);
        assertTrue("Naprawili paginator", part1.get(0).getItemId().equals(part2.get(0).getItemId()));
    }

    @Test
    public void test_GetVotesData() {
        
        Vote vote = new Vote(Arrays.asList("1048","1454108400000","8","0","null","0"));
        assertTrue("ID filmu jest nieprawidłowe", vote.getItemId().equals(1048L));
        
        LocalDate date = LocalDate.parse("2016-01-30");
        assertTrue("Nieprawidlowa data", vote.getDate().equals(date));
        
        assertTrue("Ocena filmu jest nieprawidłowa", vote.getRate() == 8);
        assertTrue("Typ oceny jest nieprawidłowy (film)", vote.getType().equals(ItemType.FILM));
        
        vote = new Vote(Arrays.asList("37003","1454108400000","9","1","fajny serial","1"));
        assertTrue("Typ oceny jest nieprawidłowy (serial)", vote.getType().equals(ItemType.SERIES));
        assertTrue("Nieprawidłowo rozpoznano czy pozycja dodana do ulubionych", vote.isFavourite());
        assertTrue("Nieprawidłowy komentarz", vote.getComment().equals("fajny serial"));
        
        vote = new Vote(Arrays.asList("609735","1454108400000","10","1","null","2"));
        assertTrue("Typ oceny jest nieprawidłowy (gra)", vote.getType().equals(ItemType.GAME));
    }
    
    @Test
    public void testGetWatchlist() throws FilmwebException {
        fa.getUserWatchlist(user.getId(), 0, 20);
    }

    @Test
    public void testGetWatchlistData() {
        WatchlistItem watchlistItem = new WatchlistItem(Arrays.asList("714192","1454108400000","3","0"));
        assertTrue("ID filmu jest nieprawidłowe", watchlistItem.getItemId().equals(714192L));
        assertTrue("Poziom zainteresowania filmu jest nieprawidłowy", watchlistItem.getLevel() == 3);
        assertTrue("Typ jest nieprawidłowy (film)", watchlistItem.getType().equals(ItemType.FILM));

        LocalDate date = LocalDate.parse("2016-01-30");
        assertTrue("Nieprawidlowa data", watchlistItem.getDate().equals(date));

        watchlistItem = new WatchlistItem(Arrays.asList("476848","1454108400000","5","1"));
        assertTrue("Typ jest nieprawidłowy (serial)", watchlistItem.getType().equals(ItemType.SERIES));
        assertTrue("Poziom zainteresowania filmu jest nieprawidłowy", watchlistItem.getLevel() == 5);

        watchlistItem = new WatchlistItem(Arrays.asList("608109","1454108400000","2","2"));
        assertTrue("Typ oceny jest nieprawidłowy (gra)", watchlistItem.getType().equals(ItemType.GAME));
        assertTrue("Poziom zainteresowania filmu jest nieprawidłowy", watchlistItem.getLevel() == 2);
    }
    
    @Test
    public void testGetUserTvChannels() throws FilmwebException {
        fa.getUserTvChannels(user.getId());
    }
}